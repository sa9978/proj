from django.http import JsonResponse
import redis
from django.conf import settings

def test_redis_connection(request):
    r = redis.Redis(
        host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB,
        charset="utf-8", decode_responses=True
    )
    r.set('foo', 'bar')
    value = r.get('foo')
    return JsonResponse({'foo': value})
