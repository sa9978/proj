from django.urls import path, include
from .views import BoardDetail, BoardList, BoardStar, ItemList, ItemDetail, ListShow, ListDetail, LabelList, LabelDetail, CommentList, CommentDetail, AttachmentDetail, AttachmentList, SubTaskList, SubTaskDetail

urlpatterns = [
    path('', BoardList.as_view(), name='board-list'),
    path('<int:pk>/', BoardDetail.as_view(), name='board-detail'),
    path('star/', BoardStar.as_view(), name='board-star'),
    path('items/', ItemList.as_view(), name='item-list'),
    path('items/<int:pk>/', ItemDetail.as_view(), name='item-detail'),
    path('lists/', ListShow.as_view(), name='list-show'),
    path('lists/<int:pk>/', ListDetail.as_view(), name='list-detail'),
    path('labels/', LabelList.as_view(), name='label-list'),
    path('labels/<int:pk>/', LabelDetail.as_view(), name='label-detail'),
    path('comments/', CommentList.as_view(), name='comment-list'),
    path('comments/<int:pk>/', CommentDetail.as_view(), name='comment-detail'),
    path('attachments/', AttachmentList.as_view(), name='attachment-list'),
    path('attachments/<int:pk>/', AttachmentDetail.as_view(), name='attachment-detail'),
    path('items/<int:task_pk>/subtasks/', SubTaskList.as_view(), name='subtask-list'),
    path('items/<int:task_pk>/subtasks/<int:pk>/', SubTaskDetail.as_view(), name='subtask-detail'),
]
